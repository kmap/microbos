#!/bin/env make -f

SHELL = /bin/sh
COMPOSE = /usr/bin/docker-compose
COMPOSE_RUN = $(COMPOSE) ${COMPOSE_ENVARGS} run --rm

COMPOSE_ENVARGS = $(if ${CONFIG},--env-file "${CONFIG}")

NAME := MicrobiOS
ID := microbios

CONFIG ?= $(shell test -r local/default.env && echo local/default.env || echo)

allisofiles = $(wildcard out/*.iso)
isofile = $(lastword $(allisofiles))

.PHONY: all
all: | build

.PHONY: build
build:
	@echo "Building ${NAME} using docker-compose..."
	$(COMPOSE) --version
	$(COMPOSE_RUN) --build microbios
	@echo "ok"

.PHONY: clean
clean:
	-sudo rm -vf -- $(allisofiles)

.PHONY: debug-docker
debug-docker:
	$(COMPOSE_RUN) --entrypoint bash microbios

.PHONY: help
help:
	@echo "$$HELP_MSG"

.PHONY: journal
journal:
	$(call ssh_tty) journalctl -xef

.PHONY: journal-export
journal-export: D ?=
journal-export:
	test -n "${D}" # D: output file name without the extension
	$(call ssh_notty) journalctl -o export \
		| /usr/lib/systemd/systemd-journal-remote -o "${D}.journal" -

.PHONY: run
run:
	test -n "$(isofile)"
	run_archiso -u -i $(isofile)

.PHONY: scp
scp: S ?=
scp: D ?=
scp: SSH_USER ?= root
scp: SSH_GROUP ?= ${SSH_USER}
scp:
	test -n "${S}" # S: source path
	test -n "${D}" # D: target destination path
	$(call ssh_notty) sudo rm -rf /tmp/remote-files
	$(call ssh_notty) mkdir -p /tmp/remote-files
	$(call source_ssh); \
		scp -F ssh_config \
			-i "$${SSH_PRIVKEY_FILE}" \
			-P 60022 \
			-r \
			${S} \
			"admin@localhost:/tmp/remote-files"
	$(call ssh_notty) sudo chown -R ${SSH_USER}:${SSH_GROUP} '/tmp/remote-files/*'
	$(call ssh_notty) sudo mv -vf '/tmp/remote-files/*' "${D}"

.PHONY: ssh
ssh:
	$(call ssh_tty)

.PHONY: version
version:
	@git describe --always --abbrev

define HELP_MSG :=
TARGETS:
  help __________________ show this help message
  all ___________________ run the default target (build)
  build _________________ build the image using the Docker Compose service
  clean _________________ remove ISO(s) from the output directory
  debug-docker __________ execute a shell in the docker container
  journal _______________ SSH into the QEMU instance and start following the journal
  journal-export ________ Export the journal from the QEMU instance
                          D: output file destination without the extension
  run ___________________ run the image in QEMU
  scp ___________________ copy file(s) into the QEMU instance via SCP
                          S: source file(s)
                          D: target destination
                          SSH_USER: user to set as owner of the file(s) [default: "root"]
                          SSH_GROUP: group to set as owner of the file(s) [default: "root"]
  ssh ___________________ log into the QEMU instance via SSH
  version _______________ print the project version/revision

OPTIONS:
  CONFIG ________________ path to environment config file [default: "local/default.env"]
endef
export HELP_MSG

define source_ssh
	test -r "${CONFIG}"
	source "${CONFIG}"
endef

define ssh_notty
	$(call _ssh_base) -T admin@localhost
endef

define ssh_tty
	$(call _ssh_base) -t admin@localhost
endef

define _ssh_base
	$(call source_ssh); \
		ssh -q \
			-F ssh_config \
			-i "$${SSH_PRIVKEY_FILE}" \
			-p 60022
endef
