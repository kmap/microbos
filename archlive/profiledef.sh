#!/usr/bin/env bash
# shellcheck disable=SC2034

iso_name="microbios"
iso_label="MICROBIOS_$(date +%Y%m)"
iso_publisher="Nicholas R. Hein <https://paradoxdev.com>"
iso_application="MicrobiOS Live ISO"
iso_version="$(date +%Y.%m.%d)"
install_dir="mkarchiso"
buildmodes=('iso')
bootmodes=('bios.syslinux.mbr' 'bios.syslinux.eltorito'
           'uefi-ia32.grub.esp' 'uefi-x64.grub.esp'
           'uefi-ia32.grub.eltorito' 'uefi-x64.grub.eltorito')
arch="x86_64"
pacman_conf="pacman.conf"
airootfs_image_type="squashfs"
airootfs_image_tool_options=('-comp' 'xz' '-Xbcj' 'x86' '-b' '1M' '-Xdict-size' '1M')
file_permissions=(
  ["/etc/shadow"]="0:0:0400"
  ["/etc/gshadow"]="0:0:0400"
  ["/etc/ssh/authorized_keys"]="0:0:0444"
  ["/etc/sudoers"]="0:0:0400"
  ["/etc/sudoers.d"]="0:0:0750"
  ["/etc/sudoers.d/wheel.sudoers"]="0:0:0400"
  ["/root"]="0:0:0750"
)
