# Advanced tab completion
autoload -U compinit
compinit
zstyle ':completion:*:descriptions' format '%U%B%d%b%u'
zstyle ':completion:*:warnings' format '%BSorry, no matches for: %d%b'

setopt correctall
setopt autocd
setopt extendedglob

PS1='%n@%m %d> '

alias ls='ls --color=auto'
alias ll='ls -lah --color=auto '
