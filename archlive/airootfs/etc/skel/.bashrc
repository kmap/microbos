alias ls='ls --color=auto'
alias ll='ls -lah --color=auto '

PS1='[\u@\h \W]\$ '
