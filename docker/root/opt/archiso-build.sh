#!/bin/bash

set -o errexit
set -o pipefail

function die() { echo -e "$@" >&2; exit 1; }

df -ah
findmnt -A

# Check directories

readonly basedir=${1:-${PWD:?}}
[ -r "$basedir" ] && [ -w "$basedir" ] || die "no read/write access to base directory: ${basedir@Q}"

readonly srcdir=${basedir}/src
readonly outdir=${basedir}/out
readonly workdir=`mktemp --directory --tmpdir XXXXXXXXXX`

[ -r "$srcdir" ] || die "no read access to source directory: ${srcdir@Q}"
[ -r "$outdir" ] && [ -w "$outdir" ] || die "no read/write access to output directory: ${outdir@Q}"
[ -r "$workdir" ] && [ -w "$workdir" ] || die "no read/write access to working directory: ${workdir@Q}"

# Build

cd "$basedir"
/usr/bin/time --format='%E' --quiet \
	/usr/bin/mkarchiso -v -w "$workdir" -o "$outdir" "$srcdir"
