# MicrobiOS

An ArchLinux variant used as the OS for KMap.

This project uses a Docker container to build the ISO file for the following reasons:

- Automated Reliability: sets-up the build to mount the source read-only and
  mount the working directory inside the host's `/tmp` directory (using tmpfs
  speeds up build times).  And all this is done automatically, and this
  consistency helps avoid some costly mistakes.
- No `sudo`: if your user is added to the "docker" group, the build context is
  sent to the Docker daemon without needing root privileges.  And the less
  `sudo` the better!
- Portability: using a Docker container means builds could hypothetically be
  run on any Linux distribution.
- Safety: `archiso` does some funky mounting when building images and that
  means one must be careful when builds fail (eg. when removing the working
  directory[^1]).  Running the build in a container mitigates *some* of this
  risk.

[^1]: https://wiki.archlinux.org/title/Archiso#Removal_of_work_directory

## Building

__Using the Docker Compose service:__

```bash
make all
```

__Cleaning up afterwards__

```bash
make clean
```

## Testing

__Running an ISO in QEMU:__

```bash
make run
```

### Tips & Tricks

* *Exit `weston`*: press `<M-C-BS>` (Meta/ALT, Control, Backspace) at the same time

## Resources

* __Weston__
  - [Manual](https://wayland.pages.freedesktop.org/weston)
